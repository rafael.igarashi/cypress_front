describe('First Test', () => {
  it('Should visit', () => {
    cy.visit('/')
  })
  it("Page should contains 'count'", () => {
    cy.contains("count")
  })
  it("should click on count and add one to the counter", () => {
    cy.contains("count")
    .click()
  })
  it('Counter should be increased',() => {
    cy.get('.counter')
    .contains("1")
  })
  it("should double click on count and add two to the counter", () => {
    cy.contains("count")
    .dblclick()
  })
  it('Counter should be 3',() => {
    cy.get('.counter')
    .contains("3")
  })
  it("Should type 'teste' on username", () => {
    cy.get('.username')
    .type("teste")
  })
  it("Should type 'teste' on password", () => {
    cy.get('.password')
    .type("teste")
  })
  it("Should submit form", () => {
    cy.get('form').submit() 
    cy.on('window:alert', (str) => {
      expect(str).to.contains(`created!`)
    })
  })
})