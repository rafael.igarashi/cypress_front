import { defineStore } from "pinia"; // import defineStore to setup the store
import Kanji from '../interfaces/Kanji' // import Kanji interface, used for each card

// initialize and export the store managment
export const useCounter = defineStore("counter", {
  state: () => {
    return {
      counter: 0
    }
  },
  // define the methods 
  actions: {
    addCounter(){
        this.counter += 1
    }
  }
})