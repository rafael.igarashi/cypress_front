import axios from "axios";
const apiConfig = {
  baseURL: 'http://localhost:3002/',
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
    'X-Requested-With': 'XMLHttpRequest'
  }
}

const api = axios.create(apiConfig)
api.interceptors.request.use((config) => {
  if (!config?.headers) {
    throw new Error(`Expected 'config' and 'config.headers' not to be undefined`);
  }
  const accessToken = localStorage.getItem('pac_teal_mapping_jwt');
  if (accessToken) {
    config.headers.Authorization = `Bearer ${accessToken}`;
  }
  return config;
});

export default api